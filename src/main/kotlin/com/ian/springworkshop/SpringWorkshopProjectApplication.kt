package com.ian.springworkshop

import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringWorkshopProjectApplication : CommandLineRunner {
  override fun run(vararg args: String?) {
    println("Hello World")
  }
}

fun main(args: Array<String>) {
  runApplication<SpringWorkshopProjectApplication>(*args)
}
